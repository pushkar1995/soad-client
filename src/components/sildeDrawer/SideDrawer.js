import React, { Component } from "react";

import "./SideDrawer.css";

import { Link } from "react-router-dom";

class SideDrawer extends Component {
  render() {
    return (
    <div>
       {/* Sidebar */}
        <nav className="w3-sidebar w3-light-grey w3-bar-block" style={{width: '20%'}}>
        <h3 className="w3-bar-item">Dhakal Non Veg Center</h3>
        <div className="navbar-nav">
        <Link className="w3-bar-item w3-button" to="/dashboard">Dashboard</Link>

        <Link to="/mainBillHeader" className="w3-bar-item w3-button">Main Bill Header</Link>

        <Link to="/billDetailsHeader" className="w3-bar-item w3-button">Bill Details Header</Link>

        <Link to="/purchaseEntryPage" className="w3-bar-item w3-button">Purchase Entry Page</Link>

        <Link to="/salesEntryPage" className="w3-bar-item w3-button">Sales Entry Page</Link>
     
        <Link to="/addCreditors" className="w3-bar-item w3-button">Add Creditors</Link>
        
        <Link to="/debtorsPage" className="w3-bar-item w3-button">Add Debtors</Link>

        <Link to="/paymentEntryPage" className="w3-bar-item w3-button">Payment Entry Page</Link>

        <Link to="/receiptEntryPage" className="w3-bar-item w3-button">Receipt Entry Page</Link>

        <Link to="/addProduct" className="w3-bar-item w3-button">Add Product Page</Link>

        <Link to="/loginPage" className="w3-bar-item w3-button">Login Page</Link>

        <Link to="/landingPage" className="w3-bar-item w3-button">Landing Page</Link>

        <Link to="/welcomePage" className="w3-bar-item w3-button">Welcome Page</Link>
        </div>
        </nav> 
    </div>
    )
  }
}
export default SideDrawer;
