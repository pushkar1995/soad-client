import React from 'react';

import { Provider } from 'react-redux';
 import store  from '../store';


import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SideDrawer from '../components/sildeDrawer/SideDrawer';
import LandingPage from '../pages/EntryPages/landingPage/LandingPage';
import LoginPage from '../pages/EntryPages/loginPage/LoginPage';
import Dashboard from '../pages/EntryPages/dashboard/Dashboard';
import WelcomePage from '../pages/EntryPages/welcomePage/WelcomePage';
import DebtorsPage from '../pages/sundryDebtors/DebtorsPage';
import CreditorsPage from '../pages/sundryCreditors/CreditorsPage';
import PaymentEntryPage from '../pages/paymentEntryPage/PaymentEntryPage';
import ReceiptEntryPage from '../pages/receiptEntryPage/ReceiptEntryPage';
import PurchaseEntryPage from '../pages/purchaseEntryPage/PurchaseEntryPage';
import SalesEntryPage from '../pages/salesEntryPage/SalesEntryPage';
import MainBillHeader from '../components/headers/mainBillHeader/MainBillHeader';
import BillDetailsHeader from '../components/headers/billDetailsHeader/BillDetailsHeader';
import AddProduct from '../pages/products/addProduct/AddProduct';

const App = () => {
    return (
        <Provider store={store}>
            <Router>
            <div>
                <SideDrawer />
                <Switch>
                <Route path="/dashboard" exact component={Dashboard} />
                <Route path="/landingPage" component={LandingPage} />
                <Route path="/welcomePage" component={WelcomePage} />
                <Route path="/loginPage" component={LoginPage} />
                <Route path="/purchaseEntryPage" component={PurchaseEntryPage} />
                <Route path="/salesEntryPage" component={SalesEntryPage} />
                <Route path="/debtorsPage" component={DebtorsPage} /> 
                <Route path="/addCreditors" component={CreditorsPage} />
                <Route path="/paymentEntryPage" component={PaymentEntryPage} />
                <Route path="/receiptEntryPage" component={ReceiptEntryPage} />
                <Route path="/addProduct" component={AddProduct} />
                <Route path="/mainBillHeader" component={MainBillHeader} />
                <Route path="/billDetailsHeader" component={BillDetailsHeader} />
                </Switch>    
            </div>
            </Router>
        </Provider>
        
    )
}

export default App;