import React, { Component } from "react";

import "./BillDetailsHeader.css";

class BillInfoHeader extends Component {
  render() {
    return (
      <div className="mainBillInfoHeaderStyles">
        <div className="billInfoHeaderContainer">
          <div className="nameStyles">
            <div className="nameTextStyle">Name:</div>
            <input type="text" className="nameInputField"></input>
          </div>

          <div className="addressInvDateContainer">
            <div className="addressStyles">
              <div className="addressTextStyle">Address:</div>
              <input type="text" className="addressInputField"></input>
            </div>

            <div className="invDateStyles">
              <div className="invDateTextStyle">Inv Date:</div>
              <input type="text" className="invDateInputField"></input>
            </div>
          </div>

          <div className="tPinNoInvNoContainer">
            <div className="tPinNoStyles">
              <div className="tPinNoTextStyle">Buyer's T-PIN No.:</div>
              <input type="text" className="tPinNoInputField"></input>
            </div>

            <div className="invNoStyles">
              <div className="invNoTextStyle">Inv No.:</div>
              <input type="text" className="invNoInputField"></input>
            </div>
          </div>

          <div className="paymentModeStyle">
            <div className="paymentModeTextStyle">Mode of payment:</div>
            <div className="dropdownStyle">
              <button className="dropbtn">Payment Type</button>
              <div className="dropdown-content">
                <a href="#">Cash</a>
                <a href="#">Credit</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default BillInfoHeader;
