import React, { Component } from "react";

import "./PurchaseEntryPage.css";

class PurchaseEntry extends Component {
  render() {
    return (
      <div className="purchaseEntryMainContainer">
        <h5 className="formHeader">
          Enter your Purchase Entries in the table.
        </h5>
        <div className="purchaseEntryFormContainer">
          <input
            type="text"
            className="particularInputField"
            placeholder="Particulars"
          ></input>

          <input
            type="text"
            className="quantityInputField"
            placeholder="Qty"
          ></input>

          <input
            type="text"
            className="measureUnitInputField"
            placeholder="MU"
          ></input>

          <input
            type="text"
            className="rateInputField"
            placeholder="Rate"
          ></input>

          <input
            type="text"
            className="vatInputField"
            placeholder="VAT"
          ></input>

          <input
            type="text"
            className="amountInputField"
            placeholder="Amount"
          ></input>

          <button className="addButtonStyle">+</button>
        </div>

        <div className="purchaseTableContainer">
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="resultDisplayContainer">
          <div className="outputContainer">
            <div className="nonTaxableStyle">
              <div className="nonTaxableNameTextStyle">Non-Taxable Value:</div>
              <input type="text" className="nonTaxableInputField"></input>
            </div>

            <div className="taxableStyle">
              <div className="taxableNameTextStyle">Taxable Value:</div>
              <input type="text" className="taxableInputField"></input>
            </div>

            <div className="vatStyle">
              <div className="vatNameTextStyle">VAT:</div>
              <input type="text" className="vatInputField"></input>
            </div>

            <div className="subTotalStyle">
              <div className="subTotalNameTextStyle">Sub Total:</div>
              <input type="text" className="subTotalInputField"></input>
            </div>

            <div className="billTotalStyle">
              <div className="billTotalNameTextStyle">Bill Total:</div>
              <input type="text" className="billTotalInputField"></input>
            </div>
          </div>

          <div className="buttonsContainer">
            <button className="backButtonStyle">Back</button>
            <button className="updateButtonStyle">Update</button>
            <button className="addNewButtonStyle">Add New</button>
            <button className="saveButtonStyle">Save</button>
            <button className="deleteButtonStyle">Delete</button>
          </div>
        </div>

        <div className="submitButtonContainer">
          <button className="submitButtonStyle">Submit</button>
        </div>
      </div>
    );
  }
}
export default PurchaseEntry;
