import React, { Component } from "react";

import "./AddProduct.css";

class AddProduct extends Component {
  render() {
    return (
      <div className="mainAddProductContainer">
        <header>I am header</header>
        <div className="addProductContainerStyles">
          <div className="addProductHeading">
            <h1 className="addProductTitle">Add Product</h1>
          </div>

          <div className="productContainer">
            <div className="addProductBody">
              <div className="productCodeStyle">
                <div className="productCodeTextStyle">Product Code:</div>
                <input type="text" className="productCodeInputField"></input>
              </div>

              <div className="productNameStyle">
                <div className="productNameTextStyle">Product Name:</div>
                <input
                  type="text"
                  className="productNameInputFieldStyle"
                ></input>
              </div>

              <div className="muStyle">
                <div className="muTextStyle">Measuring Unit:</div>
                <div className="dropdownStyle">
                  <button className="dropbtn">M.U.</button>
                  <div className="dropdown-content">
                    <a href="#">Kg</a>
                    <a href="#">Carate</a>
                    <a href="#">packets</a>
                    <a href="#">Others</a>
                    <a href="#">Unit</a>
                  </div>
                </div>
              </div>
            </div>

            <div className="dividerLine"></div>

            <div className="productFooter">
              <button className="cancelButtonStyle">Cancel</button>
              <button className="saveButtonStyle">Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AddProduct;
