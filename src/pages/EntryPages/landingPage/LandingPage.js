import React from 'react'

import "./LandingPage.css";
import SoftwareLogo from "../../../resources/LogoImages/SoftwareLogo.jpg";


const LandingPage = () => {
    return (
    <div className="landingPageContainer">
        <img src={SoftwareLogo} alt="" className="softwareLogoStyle" />
    </div>
    )
}

export default LandingPage
