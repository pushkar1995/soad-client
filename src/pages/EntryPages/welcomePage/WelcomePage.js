import React from 'react'

import "./WelcomePage.css";
import SoftwareLogo from "../../../resources/LogoImages/SoftwareLogo.jpg";
import CompanyLogo from "../../../resources/LogoImages/CompanyLogo.jpg";

const WelcomePage = () => {
    return (
        <div className="welcomePageContainer">
        <div className="welcomeHeadingContainer">
          <img src={SoftwareLogo} alt="" className="softwareLogoStyles" />
          <h1 className="welcomeHeading">WELCOME TO SOAD</h1>
        </div>
        <div className="plsHeading">
          <h2>Please Choose Your Company</h2>
        </div>
        <div className="companyContainer">
          <div className="companyStyle">
            <h3 className="companyName">Dhakal NonVeg Center</h3>
            <img src={CompanyLogo} alt="" className="companyLogoStyles" />
          </div>
        </div>
      </div>
    )
}

export default WelcomePage

