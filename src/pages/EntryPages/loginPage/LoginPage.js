import React from "react";

import './LoginPage.css'
import LoginForm from "./LoginForm/LoginForm";


const LoginPage = () => {
    return (
        <div className="loginPageContainer">
             <LoginForm />
        </div>
    )
}

export default LoginPage;
