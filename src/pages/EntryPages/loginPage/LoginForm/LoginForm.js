import React, { Component } from "react";

import "./LoginForm.css";

class LoginForm extends Component {
  render() {
    return (
      <div className="loginMainFormContainer">
        <div className="circleStyle">
          <h3 className="softwareName">SOAD</h3>
        </div>
        <div className="loginFormContainer">
          <form>
            <h1 className="loginHeader">Login</h1>
            <div class="form-group">
              <input
                type="email"
                class="form-control"
                placeholder="Username or Email"
              />
            </div>

            <div class="form-group">
              <input
                type="password"
                class="form-control"
                placeholder="Password"
              />
            </div>

            <div class="form-check">
              <input type="checkbox" class="form-check-input" />
              <label class="form-check-label">Keep me Sign In</label>
            </div>
            <button type="submit" class="btn-primary-signin">
              Sign In
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default LoginForm;

