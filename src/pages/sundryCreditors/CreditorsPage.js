import React, { Component } from "react";

import "./CreditorsPage.css";

class Creditors extends Component {
  render() {
    return (
      <div className="mainSundryCreditorsContainer">
        <header>I am header</header>
        <div className="sundryCreditorsContainerStyles">
          <div className="sundryCreditorsHeading">
            <h1 className="sundryCreditorsTitle">Sundry Creditors</h1>
          </div>

          <div className="creditorsContainer">
            <div className="creditorsBody">
              <div className="creditorsNameStyle">
                <div className="creditorsNameTextStyle">Creditors Name:</div>
                <input type="text" className="creditorsNameInputField"></input>
              </div>

              <div className="creditorsPinStyle">
                <div className="tPinTextStyle">Tpin No.:</div>
                <input type="text" className="tPinInputFieldStyle"></input>
              </div>

              <div className="addressStyle">
                <div className="addressTextStyle">Address:</div>
                <input type="text" className="addressFieldStyle"></input>
              </div>

              <div className="contactStyle">
                <div className="contactTextStyle">Contact Numbers:</div>
                <input type="text" className="contactFieldStyle"></input>
                <input type="text" className="contactFieldStyle"></input>
              </div>

              <div className="relPersonStyle">
                <div className="relPersonTextStyle">Releted Person:</div>
                <input type="text" className="relPersonFieldStyle"></input>
              </div>

              <div className="degiStyle">
                <div className="degiTextStyle">Degignation:</div>
                <input type="text" className="degiFieldStyle"></input>
              </div>
            </div>
          </div>
          <div className="dividerLine"></div>

          <div className="creditorsFooter">
            <button className="cancelButtonStyle">Cancel</button>
            <button className="saveButtonStyle">Save</button>
          </div>
        </div>
      </div>
    );
  }
}
export default Creditors;
