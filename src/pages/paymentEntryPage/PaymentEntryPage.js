import React, { Component } from "react";

import "./PaymentEntryPage.css";

class PaymentEntry extends Component {
  render() {
    return (
      <div className="mainContainerStyles">
        <header>I am header</header>
        <div className="payContainerStyles">
          <div className="payHeading">
            <h1 className="payTitle">Payment Entry</h1>
          </div>
          <div className="dateContainer">
            <div>Entry Date:</div>
            <div>Receipt Date:</div>
          </div>
          <div className="dividerLine"></div>
          <div className="payBodyContainer">
            <div className="payBody">
              <div className="payFromStyle">
                <div className="payFromTextStyle">Payment from:</div>
                <input type="text" className="payFromInputField"></input>
              </div>
              <div className="payByStyle">
                <div className="payByTextStyle">Payment By:</div>
                <div className="dropdownStyle">
                  <button className="dropbtn">Payment Type</button>
                  <div className="dropdown-content">
                    <a href="#">Cash</a>
                    <a href="#">Cheque</a>
                    <a href="#">Bank Transfer</a>
                    <a href="#">Others</a>
                  </div>
                </div>
              </div>
              <div className="payAmountStyle">
                <div className="payAmountTextStyle">Payment Amount:</div>
                <input type="text" className="payAmountInputField"></input>
              </div>

              <div className="payAmountWordStyle">
                <div className="payAmountWordTextStyle">Amount in words:</div>
                <input type="text" className="payAmtWordInputField"></input>
              </div>
              <div className="payRemarksStyle">
                <div className="payRemarkTextStyle">Remarks:</div>
                <input type="text" className="payRemarksInputField"></input>
              </div>
            </div>
            <div className="dividerLine"></div>
            <div className="payFooter">
              <button className="cancelButtonStyle">Cancel</button>
              <button className="saveButtonStyle">Save</button>
              <button className="printButtonStyle">Print</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default PaymentEntry;
