import { combineReducers } from 'redux';
import addProductReducer from './addProductReducer';

export default combineReducers({
    addProduct: addProductReducer
})